package test.tuananhlgs.food;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

public class RecipeActivity extends AppCompatActivity {

    ArrayList<Recipe> recipeList;
    ListView lvrecipe;
    RecipeAdapter adapter1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        Mapped();

//        setTitle("Recipe");
//        getActionBar().setIcon(R.drawable.icon);

        adapter1 = new RecipeAdapter(this, R.layout.recipe_line, recipeList);
        lvrecipe.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();
    }

    private void Mapped() {
        lvrecipe = (ListView) findViewById(R.id.list_recipe);

        recipeList = new ArrayList<>();
        recipeList.add(new Recipe("Mandarian Sorbet", "by John Doe", R.drawable.img1, R.drawable.user1));
        recipeList.add(new Recipe("Blackberry Sorbet", "by John Doe", R.drawable.img2, R.drawable.user1));
        recipeList.add(new Recipe("Mango Sorbet", "by John Doe", R.drawable.img3, R.drawable.user1));
        recipeList.add(new Recipe("Strawberry Sorbet", "by John Doe", R.drawable.img4, R.drawable.user1));
        recipeList.add(new Recipe("Apple Sorbet", "by John Doe", R.drawable.img5, R.drawable.user1));
    }
}
