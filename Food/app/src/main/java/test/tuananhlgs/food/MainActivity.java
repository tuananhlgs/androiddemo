package test.tuananhlgs.food;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<DrawerAction> actionList;
    DrawerActionAdapter adapter;
    ListView lvdrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Mapped();

//        setTitle("Menu");
//        getActionBar().setIcon(R.drawable.icon);
//        getActionBar().setBackgroundDrawable(R.drawable.actionbar_bg);

        adapter = new DrawerActionAdapter(this, R.layout.drawer_action, actionList);
        lvdrawer.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        lvdrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 2){
                    Intent intent1 = new Intent(MainActivity.this, RecipeActivity.class);
                    startActivity(intent1);
                }
                else {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void Mapped() {
        lvdrawer = (ListView) findViewById(R.id.leftDrawer);
        actionList = new ArrayList<>();
        actionList.add(new DrawerAction("Home", R.drawable.ic_nav1));
        actionList.add(new DrawerAction("Compose", R.drawable.ic_nav2));
        actionList.add(new DrawerAction("Recipes", R.drawable.ic_nav3));
        actionList.add(new DrawerAction("Categories", R.drawable.ic_nav4));
        actionList.add(new DrawerAction("Profile", R.drawable.ic_nav5));
        actionList.add(new DrawerAction("Setting", R.drawable.ic_nav6));

    }
}
