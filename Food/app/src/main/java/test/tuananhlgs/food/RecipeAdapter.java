package test.tuananhlgs.food;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by DELL-PC on 7/13/2017.
 */

public class RecipeAdapter extends BaseAdapter {

    private Context context;
    int layout;
    private List<Recipe> recipes;

    public RecipeAdapter(Context context, int layout, List<Recipe> recipes) {
        this.context = context;
        this.layout = layout;
        this.recipes = recipes;
    }

    @Override
    public int getCount() {
        return recipes.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class ViewHolder{
        TextView txtFood, txtChef;
        ImageView imgFood, imgUser;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = new ViewHolder();

        if (view == null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(layout, null);

            holder.imgFood = (ImageView) view.findViewById(R.id.image_food);
            holder.imgUser = (ImageView) view.findViewById(R.id.image_user);
            holder.txtFood = (TextView) view.findViewById(R.id.name_food);
            holder.txtChef = (TextView) view.findViewById(R.id.chef);

            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }

        Recipe recipe = recipes.get(i);
        holder.imgFood.setImageResource(recipe.getFoodImage());
        holder.imgUser.setImageResource(recipe.getUserImage());
        holder.txtFood.setText(recipe.getFoodName());
        holder.txtChef.setText(recipe.getChef());

        return view;
    }
}
